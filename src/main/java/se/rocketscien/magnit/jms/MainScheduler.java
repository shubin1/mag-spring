package se.rocketscien.magnit.jms;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@RequiredArgsConstructor
@Component
public class MainScheduler {

    private final Sender sender;

    private final AtomicInteger atomicInteger = new AtomicInteger(0);

    @Scheduled(fixedRate = 5000)
    public void tick() {
        sender.send(String.format("%d", atomicInteger.addAndGet(1)));
    }

}
