package se.rocketscien.magnit.jms;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class Sender {

    private final JmsTemplate jmsTemplate;

    public void send(String message) {
        log.info("Sending message: {}", message);

        jmsTemplate.convertAndSend("test-destination", message);
    }
}
