package se.rocketscien.magnit.jms;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.rocketscien.magnit.model.Header;
import se.rocketscien.magnit.model.MessageText;
import se.rocketscien.magnit.repository.HeaderRepository;
import se.rocketscien.magnit.repository.MessageTextRepository;

import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
@Component
public class Consumer {

    private final HeaderRepository headerRepository;
    private final MessageTextRepository messageTextRepository;

    @JmsListener(destination = "test-destination")
    @Transactional
    public void consume(@Payload String message, MessageHeaders messageHeaders) {
        log.info("Message received: {}", message);

        if (message.equals("-1")) {
            throw new RuntimeException("message is -1");
        }

        MessageText newMessageText = messageTextRepository.save(
                MessageText.content(
                        message.substring(0, Math.min(message.length(), MessageText.TEXT_VALUE_SIZE))
                )
        );
        Long newMessageId = newMessageText.getId();

        messageHeaders.keySet().forEach(headerName -> {
            String headerValue = Objects.toString(messageHeaders.get(headerName), "null");
            headerRepository.save(
                    Header.content(
                            headerName.substring(0, Math.min(headerName.length(), Header.HEADER_NAME_SIZE)),
                            headerValue.substring(0, Math.min(headerValue.length(), Header.HEADER_VALUE_SIZE)),
                            newMessageId
                    )
            );
        });
    }
}
