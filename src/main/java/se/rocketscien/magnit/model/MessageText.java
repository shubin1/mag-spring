package se.rocketscien.magnit.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Getter
@AllArgsConstructor
@Table("message_text")
public class MessageText {

    public static final int TEXT_VALUE_SIZE = 256;

    @Id
    private final Long id;

    private final String textValue;

    public static MessageText content(String textValue) {
        return new MessageText(null, textValue);
    }

    public MessageText withId(Long id) {
        return new MessageText(id, textValue);
    }

}
