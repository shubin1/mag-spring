package se.rocketscien.magnit.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Getter
@AllArgsConstructor
@Table("header")
public class Header {

    public static int HEADER_NAME_SIZE = 64;
    public static int HEADER_VALUE_SIZE = 256;

    @Id
    private final Long id;

    private final String headerName;

    private final String headerValue;

    private final Long messageTextId;

    public static Header content(String headerName, String headerValue, Long messageTextId) {
        return new Header(null, headerName, headerValue, messageTextId);
    }

    public Header withId(Long id) {
        return new Header(id, headerName, headerValue, messageTextId);
    }

}
