package se.rocketscien.magnit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.rocketscien.magnit.model.Header;

@Repository
public interface HeaderRepository extends CrudRepository<Header, Long> {
}
