package se.rocketscien.magnit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import se.rocketscien.magnit.model.MessageText;

@Repository
public interface MessageTextRepository extends CrudRepository<MessageText, Long> {
}
