create table message_text(
    id int auto_increment not null,
    text_value varchar(256) not null,
    primary key (id)
);

create table header(
    id int auto_increment not null,
    header_name varchar(64) not null,
    header_value varchar(256) not null,
    message_text_id int not null,
    primary key (id),
    foreign key (message_text_id) references message_text(id)
);